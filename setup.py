from distutils.core import setup

setup(
    name="bliss",
    version="1.0",
    packages=["bliss"],
    description='Python Distribution Utilities',
    author='Greg Ward',
    author_email='gward@python.net',
    url='https://www.python.org/sigs/distutils-sig/',
)